" Thanks for ideas to selikapro:
" https://github.com/selikapro/vimrc-likapro
" https://www.youtube.com/watch?v=mbSaK3EOqO8


" Set mapleader to ','
let mapleader = ","

" Jedi configuration must be defined before loading the plugin
let g:jedi#popup_on_dot = 0
let g:jedi#goto_command = "<leader>*"
let g:jedi#goto_assignments_command = "<leader>jg"
let g:jedi#usages_command = "<leader>jn"
let g:jedi#rename_command = "<leader>ju"
let g:jedi#goto_stubs_command = "<leader>js"
let g:jedi#documentation_command = "<leader>jK"
let g:jedi#completions_command = "<C-n>"
                                              
" Plugins
call plug#begin('~/.vim/plugged')

" Comment and uncomment lines
Plug 'preservim/nerdcommenter'

" A light and configurable statusline/tabline plugin for Vim
Plug 'itchyny/lightline.vim'

" Directory tree
Plug 'scrooloose/nerdtree'

" Syntax highlighting for languages
Plug 'sheerun/vim-polyglot'

" Fzf is a general-purpose command-line fuzzy finder
" Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
" Plug 'junegunn/fzf.vim'

" Gruvbox color theme
Plug 'morhetz/gruvbox'

" Vim-monokai-tasty color theme
Plug 'patstockwell/vim-monokai-tasty'

" Terminal
" Plug 'voldikss/vim-floaterm'

" ctrlp
Plug 'ctrlpvim/ctrlp.vim'

" buffer tree
Plug 'jeetsukumaran/vim-buffergator'

" Code lint/fix
Plug 'dense-analysis/ale'
" autocomplete...
Plug 'davidhalter/jedi-vim'

" Make your Vim/Neovim as smart as VSCode
" Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Search with ack -> requirements: install ack-grep
Plug 'mileszs/ack.vim'

" Git
Plug 'tpope/vim-fugitive'

" Multiple cursors
Plug 'terryma/vim-multiple-cursors'

" Snippets
Plug 'SirVer/ultisnips'
" Plug 'honza/vim-snippets'

call plug#end()

" Syntax highlighting
syntax on

" let g:floaterm_keymap_new    = '<C-a>'
" let g:floaterm_keymap_prev   = '<C-n>'
" let g:floaterm_keymap_next   = '<C-n>'
" let g:floaterm_keymap_toggle = '<C-f>'
" let g:floaterm_keymap_kill = '<C-k>'

" nnoremap   <silent>   <C-a>    :FloatermNew<CR>
" tnoremap   <silent>   <C-a>    <C-\><C-n>:FloatermNew<CR>
" nnoremap   <silent>   <C-n>    :FloatermPrev<CR>
" tnoremap   <silent>   <C-n>    <C-\><C-n>:FloatermPrev<CR>
" nnoremap   <silent>   <C-n>   :FloatermNext<CR>
" tnoremap   <silent>   <C-n>   <C-\><C-n>:FloatermNext<CR>
" nnoremap   <silent>   <C-f>   :FloatermToggle<CR>
" tnoremap   <silent>   <C-f>   <C-\><C-n>:FloatermToggle<CR>
" nnoremap   <silent>   <C-k>   :FloatermKill<CR>
" tnoremap   <silent>   <C-k>   <C-\><C-n>:FloatermKill<CR>

" Set FZF Default to Ripgrep (must install ripgrep)
let $FZF_DEFAULT_COMMAND = 'rg --files --hidden --follow --no-ignore-vcs'

" Options viewable by using :options
" Set options viewable by using :set all
" Or help for individual configs can be accessed :help <name>
set nocompatible
set redrawtime=10000
set background=dark
set laststatus=2
set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set nowrap
set nobackup
set undodir=~/.vim/undordir
set undofile 
set incsearch
" set relativenumber
" set cursorline

" Column set to column 100
set colorcolumn=88

" Column color set to grey
highlight ColorColumn ctermbg=1

" Maps
" nmap <leader>hk :vsplit ~/.vim/hotkeys<cr>
nmap <leader>gd <Plug>(coc-definition)
nmap <leader>gr <Plug>(coc-references)
nmap <leader>t :NERDTreeToggle<cr>
nmap <leader><leader>p :Prettier<cr>
nmap <leader><leader>g :GoFmt<cr>
" nmap <leader><leader>b :Black<cr>
nmap <leader><leader>u :UndotreeToggle<cr>
" Files (runs $FZF_DEFAULT_COMMAND if defined)
nmap <leader><leader>f :Files<cr>
nmap <leader><leader><leader>g :GoMetaLinter<cr>
nnoremap <C-p> :GFiles<CR>
nnoremap <leader><leader>c :call NERDComment(0,"toggle")<CR>
vnoremap <leader><leader>c :call NERDComment(0,"toggle")<CR>
" nnoremap <leader><Tab> :tabprevious<CR>
nnoremap <leader><Tab> :tabnext<CR>
nnoremap <leader>n :bprev<CR>
nnoremap <leader>m :bnext<CR>

nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
nnoremap <C-h> <C-w>h

vnoremap < <gv " better indentation
vnoremap > >gv " better indentation

let NERDTreeIgnore = ['\.pyc$', '__pycache__/*']

set nobackup
set nowritebackup
set noswapfile

set bs=2
nnoremap <C-N><C-N> :set invnumber<cr>

" load up the nerd tree
autocmd vimenter * NERDTree
let g:NERDTreeWinPos = "right"

" Quit current window
nnoremap <leader>e :quit<cr>
" Quit curent window even if there are unsaved changes
nnoremap <Leader>E :q!<cr>
" Quit all windows
nnoremap <C-c> :qa!<cr>

nnoremap <C-Left> :vertical resize -5 <cr>
nnoremap <C-Right> :vertical resize +5 <cr>
nnoremap <C-Up> :resize -5 <cr>
nnoremap <C-Down> :resize +5 <cr>

nnoremap <S-j> 3j
nnoremap <S-k> 3k
nnoremap <leader>j <S-j>

" Make search case insensitive
set hlsearch                                                                            
set incsearch                                                                           
set ignorecase                                                                          
set smartcase                                                                           

" Disable backup and swap files
set nobackup
set nowritebackup
set noswapfile

autocmd VimEnter * wincmd p

" pastee toggle
set pastetoggle=<F3>

nnoremap <leader>s :update<cr>
vnoremap <leader>s <C-C>:update<cr>
inoremap <leader>s <Esc>:update<cr><esc>

" nnoremap <leader>b :Black<cr>
" inoremap <leader>b <C-O>:Black<cr><esc>

" highlight line in insert mode
:autocmd InsertEnter * set cul
:autocmd InsertLeave * set nocul

let &t_SI = "\<esc>[5 q"  " blinking I-beam in insert mode
let &t_SR = "\<esc>[3 q"  " blinking underline in replace mode
let &t_EI = "\<esc>[ q"  " default cursor (usually blinking block) otherwise

" Terminal
" set termwinsize=10x0
set termwinsize=0x50
" open terminal on the bottom of the screen
" nnoremap <C-t> :bo term<cr>
" nnoremap <C-t> :bel vert term<cr>
nnoremap <C-t> :vert term<cr>
tnoremap <C-j> <C-w>j
tnoremap <C-k> <C-w>k
tnoremap <C-l> <C-w>l
tnoremap <C-h> <C-w>h 
tnoremap <C-Left> <C-w>:vertical resize -5 <cr>
tnoremap <C-Right> <C-w>:vertical resize +5 <cr>
tnoremap <C-Up> <C-w>:resize +5 <cr>
tnoremap <C-Down> <C-w>:resize -5 <cr>
" new terminal from terminal
" tnoremap <C-t> <C-w>:vsplit<cr><C-w>:term ++curwin<cr>
tnoremap <C-t> <C-w>:split<cr><C-w>:term ++curwin<cr>
" close terminal
tnoremap <C-d> <C-w><C-c><C-d>

" Settings for ctrlp
" cd ~/.vim/bundle
" git clone https://github.com/kien/ctrlp.vim.git
let g:ctrlp_max_height = 30
let g:ctrlp_custom_ignore = 'venv*/\|node_modules/'
set wildignore+=*.pyc
set wildignore+=*_build/*
set wildignore+=*/coverage/*
set wildignore+=*/tests/*
set wildignore+=*/node_modules/*

set noequalalways
let g:buffergator_autodismiss_on_select = 0
let g:buffergator_autoupdate = 1
let g:buffergator_viewport_split_policy = "R"
let g:buffergator_suppress_keymaps = 1
let g:buffergator_split_size=25
let g:buffergator_show_full_directory_path=0
:autocmd FileType buffergator noremap <buffer> <leader>m <nop>
:autocmd FileType buffergator noremap <buffer> <leader>n <nop>
:autocmd FileType nerdtree noremap <buffer> <leader>m <nop>
:autocmd FileType nerdtree noremap <buffer> <leader>n <nop>
nnoremap <leader>b :BuffergatorToggle<cr>
augroup termIgnore
    autocmd!
    autocmd TerminalOpen * set nobuflisted
augroup END

nnoremap <S-b> 3b
nnoremap <S-e> 3e
" autocmd vimenter * BuffergatorOpen


" ---------------------- ALE ----------------------------------
let g:ale_linters = {'python': ['black', 'flake8', 'pydocstyle', 'bandit', 'mypy'], 'javascript': ['tsserver']}
let g:ale_fixers = {'python': ['black', 'isort', 'trim_whitespace', 'remove_trailing_lines']}
let g:ale_fix_on_save = 1
" let g:ale_lint_on_text_changed = 'never'
" nnoremap <leader>ad :ALEGoToDefinition<cr>
" nnoremap <leader>ar :ALEFindReference<cr>
" let g:ale_sign_column_always = 1
" let g:ale_sign_error = '>>'
" let g:ale_sign_warning = '--'


nnoremap <Leader>a :Ack<Space>

" Fugitive
set statusline=%<%f\ %h%m%r%{FugitiveStatusline()}%=%-14.(%l,%c%V%)\ %P
nnoremap <leader>ga :Gwrite<cr>
nnoremap <leader>gmv :Gmove 
nnoremap <leader>gcc :Gread<cr>
nnoremap <leader>gco :Git commit<cr>
nnoremap <leader>ghi :Git blame<cr>
nnoremap <leader>gg :Git 
" :Gedit <branch_name>:<file_name>
set diffopt+=vertical

" lightline
set noshowmode
let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'FugitiveHead'
      \ },
      \ }

let g:UltiSnipsExpandTrigger="<leader><leader>"
let g:UltiSnipsJumpForwardTrigger="<C-j>"
let g:UltiSnipsJumpBackwardTrigger="<C-k>"
set clipboard=unnamed  " Allow copy paste from external windows

" Colorscheme (For gruvbox $TERM env var needs to be xterm-256color)
autocmd vimenter * ++nested colorscheme gruvbox
"colorscheme vim-monokai-tasty
