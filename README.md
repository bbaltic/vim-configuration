VIM configuration
=================
This repository contains vimrc file for vim configuration.

Requirements
------------
- vim with python3.6+ support
- ack-grep


Optional Requirements
---------------------
ALE can work without installed linters, but without it there will be no effect.
It is recommened to start vim from environment that have access to:
- black
- flake8
- pydocstyle
- bandit
- mypy


Install plugins
---------------
```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

Enter .vimrc file and run command
```
:PlugInstall
```

Snippets
--------
Put https://github.com/honza/vim-snippets/blob/master/UltiSnips/python.snippets in
~/.vim/UltiSnips/python.snippets
